<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Survey;
use App\AnswerOption;
use App\GuestAnswer;

class GuestController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        $surveys = Survey::where('status', 1)->get();

        return view('guess/index')
            ->with('surveys', $surveys);
    }

    public function showSurvey($surveyId)
    {
        $survey = Survey::find($surveyId);
        $survey->answerOptions = AnswerOption::where('survey_id', $surveyId)->get();

        return view('guess/answer_survey')
            ->with('survey', $survey);
    }

    public function storeAnswer(Request $request, $surveyId)
    {
        $this->validate($request, [
            'answer' => 'required',
            'name' => 'required|max:75'
        ]);

        $guestAnswer = new GuestAnswer;
        $guestAnswer->name = $request->name;
        $guestAnswer->survey_id = $surveyId;
        $guestAnswer->answer_option_id = $request->answer;
        $guestAnswer->save();

        // Flag survey as answered
        $request->session()->push('answered_surveys', $surveyId);

        $request->session()->flash('success_message', 'Successfully submitted survey response.');

        return redirect()->action('GuestController@index');
    }
}
