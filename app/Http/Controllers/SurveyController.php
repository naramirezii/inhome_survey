<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Survey;
use App\AnswerOption;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::all();

        return view('survey/index')
            ->with('surveys', $surveys)
            ->with('page_title', 'Survey List');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('survey/create')
            ->with('page_title', 'Create Survey');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:surveys|max:75',
            'description' => 'max:255'
        ]);

        $survey = new Survey;
        $survey->title = $request->title;
        $survey->description = $request->description;
        $survey->save();

        $request->session()->flash('success_message', 'Successfully added record.');

        return redirect()->route('survey.edit', $survey->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = Survey::find($id);
        $answerOptions = AnswerOption::where('survey_id', $id)->get();

        $surveyResults = DB::table('answer_options')
             ->select('answer_option_id', 'answer_options.title', DB::raw('count(*) as total'))
             ->join('guest_answers', 'answer_options.id', '=', 'guest_answers.answer_option_id')
             ->where('answer_options.survey_id', $id)
             ->groupBy('answer_option_id')->get();

        return view('survey/edit')
            ->with('survey', $survey)
            ->with('answerOptions', $answerOptions)
            ->with('surveyResults', $surveyResults)
            ->with('page_title', 'Edit Survey');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:75',
            'description' => 'max:255'
        ]);

        $survey = Survey::find($id);
        $survey->title = $request->title;
        $survey->description = $request->description;
        $survey->status = $request->status;
        $survey->save();

        $request->session()->flash('success_message', 'Successfully updated record');

        return redirect()->route('survey.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createOption($surveyId)
    {
        $survey = Survey::find($surveyId);

        return view('survey/create_option')
            ->with('survey', $survey)
            ->with('page_title', 'Create Answer Option');
    }

    public function storeOption(Request $request, $surveyId)
    {
        $this->validate($request, [
            'title' => 'required|max:75',
        ]);

        $answerOption = new AnswerOption;
        $answerOption->survey_id = $surveyId;
        $answerOption->title = $request->title;
        $answerOption->save();

        $request->session()->flash('success_message', 'Successfully added record.');

        return redirect()->route('survey.edit', $surveyId);
    }

    public function editOption($surveyId, $answerOptionId)
    {
        $survey = Survey::find($surveyId);
        $answerOption = AnswerOption::find($answerOptionId);

        return view('survey/edit_option')
            ->with('survey', $survey)
            ->with('answerOption', $answerOption)
            ->with('page_title', 'Edit Answer Option');;
    }

    public function updateOption(Request $request, $surveyId, $answerOptionId)
    {
        $this->validate($request, [
            'title' => 'required|max:75',
        ]);

        $answerOption = AnswerOption::find($answerOptionId);
        $answerOption->title = $request->title;
        $answerOption->save();

        $request->session()->flash('success_message', 'Successfully updated answer option.');

        return redirect()->route('survey.edit', $surveyId);
    }
}
