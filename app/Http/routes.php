<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Guest
Route::get('/', 'GuestController@index');
Route::get('answer-survey/{id}/', 'GuestController@showSurvey');
Route::post('answer-survey/{id}/store', 'GuestController@storeAnswer');

// Survey
Route::get('survey/{id}/option/create', 'SurveyController@createOption');
Route::post('survey/{id}/option/store', 'SurveyController@storeOption');
Route::get('survey/{id}/option/{option_id}/edit', 'SurveyController@editOption');
Route::post('survey/{id}/option/{option_id}/update', 'SurveyController@updateOption');
Route::resource('survey', 'SurveyController');

// Auth
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');