<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ABC Surveys</title>
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset("css/prettyPhoto.css") }}" rel="stylesheet">
    <link href="{{ asset("css/animate.css") }}" rel="stylesheet">
    <link href="{{ asset("css/main.css") }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ asset("js/html5shiv.js") }}></script>
    <script src="{{ asset("js/respond.min.js") }}></script>
    <![endif]-->       
    <link rel="shortcut icon" href="">

    @yield('head')

    <!-- Theme style -->
    <link href="{{ asset('admin-lte/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    
    <!-- Header -->
    @include('partials/public_header')

    <!-- Main Content -->
    @yield('content')

    <!-- Footer -->
    @include('partials/public_footer')

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset ("js/jquery.js") }}"></script>
    <script src="{{ asset ("js/bootstrap.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("js/jquery.prettyPhoto.js") }}"></script>
    <script src="{{ asset ("js/main.js") }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset ("admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>

    @yield('script')    
</body>
</html>