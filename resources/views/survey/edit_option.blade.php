@extends('admin_template')

@section('head')
@endsection

@section('breadcrumbs')
	<li><a href="{{ route('survey.index') }}">Survey List</a></li>
	<li><a href="{{ route('survey.edit', $survey->id) }}">{{ $survey->title }}</a></li>
	<li class="active">Edit Answer Option</li>
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
		@include('partials.flash_message')
		<h4>Survey: {{ $survey->title }}</h4>
		
		<div class="box">
			<div class="box-body">
				
				<form action="/survey/{{$survey->id}}/option/{{$answerOption->id}}/update" method="POST">
		        	{!! csrf_field() !!}
		            
		            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
	                  <label>Title</label>
	                  <input type="text" name="title" value="{{{ old('title', $answerOption->title) }}}" class="form-control" />
	                </div>

		            <button type="submit" class="btn btn-primary">Update</button>
		            <a href="{{ route('survey.edit', $survey->id) }}" class="btn"><u>Cancel</u></a>
		        </form>	    

			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->

@endsection

@section('script')
@endsection
