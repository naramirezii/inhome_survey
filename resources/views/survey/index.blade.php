@extends('admin_template')

@section('content')
    <div class="row">
	    <div class="col-xs-12">
	    	<a href="{{ route('survey.create') }}" class="btn btn-primary btn-flat fa fa-plus"> Add New</a>
	    	<br/><br/>

	    @include('partials.flash_message')

	      <div class="box">
	        <div class="box-body">
	          <table id="data_table" class="table table-bordered table-striped">
	            <thead>
	              <tr>
	                <th>Title</th>
	                <th>Description</th>
	                <th>Status</th>
	              </tr>
	            </thead>
	            <tbody>
	            	@foreach($surveys as $survey)
		              <tr>
		                <td><a href="{{ route('survey.edit', $survey->id) }}">{{ $survey->title }}</td>
		                <td>{{ $survey->description }}</td>
		                <td>
	      					@if($survey->status == 0)
	      						<div class="label label-default">Deactivated</div>
	      					@elseif($survey->status == 1)
	      						<div class="label label-success">Active</div>
	      					@endif
	      				</td>
		              </tr>
		            @endforeach
	            </tbody>
	            <tfoot>
	              <tr>
	                <th>Title</th>
	                <th>Description</th>
	                <th>Status</th>
	              </tr>
	            </tfoot>
	          </table>
	        </div><!-- /.box-body -->
	      </div><!-- /.box -->
	    </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


@section('script')
	<!-- DATA TABES SCRIPT -->
	<script src="{{ asset("admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>

	<script type="text/javascript">
	  	// Setup - add a text input to each footer cell
	    $('#data_table tfoot th').each( function () {
	        var title = $('#data_table thead th').eq( $(this).index() ).text();
	        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	    } );

		$(function () {
			var table = $('#data_table').DataTable({
				"paging": true,
				"lengthChange": true,
				"searching": true,
				"ordering": true,
				"info": true,
				"autoWidth": true
			});

		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            that
		                .search( this.value )
		                .draw();
		        } );
		    } );

		});	
	</script>
@endsection
