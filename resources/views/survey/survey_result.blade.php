<table id="data_table2" class="table table-bordered table-striped">
	<thead>
	  <tr>
	    <th>Answer</th>
	    <th>Votes</th>
	  </tr>
	</thead>
	<tbody>
	@foreach($surveyResults as $surveyResult)
	  <tr>
	    <td>{{ $surveyResult->title }}</td>
	    <td>{{ $surveyResult->total }}</td>
	  </tr>
	 @endforeach
	</tbody>
</table>