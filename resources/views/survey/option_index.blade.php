<a href="/survey/{{ $survey->id }}/option/create" class="btn btn-primary btn-flat fa fa-plus"> Add Answer Option</a>
<br/><br/>
<table id="data_table" class="table table-bordered table-striped">
	<thead>
	  <tr>
	    <th>Title</th>
	    <th>Status</th>
	  </tr>
	</thead>
	<tbody>
	@foreach($answerOptions as $option)
	  <tr>
	    <td><a href="/survey/{{ $survey->id }}/option/{{ $option->id }}/edit">{{ $option->title }}</td>
	    <td>
			@if($option->status == 0)
				<div class="label label-default">Deactivated</div>
			@elseif($option->status == 1)
				<div class="label label-success">Active</div>
			@endif
		</td>
	  </tr>
	 @endforeach
	</tbody>
	<tfoot>
	  <tr>
	    <th>Title</th>
	    <th>Status</th>
	  </tr>
	</tfoot>
</table>