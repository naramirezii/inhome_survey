@extends('admin_template')

@section('head')
@endsection

@section('breadcrumbs')
	<li><a href="{{ route('survey.index') }}">Survey List</a></li>
	<li class="active">Edit</li>
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
		@include('partials.flash_message')
		<h4>{{ $survey->title }}</h4>

		<!-- Custom Tabs -->
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1" data-toggle="tab">Survey Details</a></li>
				<li><a href="#tab_2" data-toggle="tab">Answer Options</a></li>
				<li><a href="#tab_3" data-toggle="tab">Survey Results</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box">
						<div class="box-body">
							<form action="{{ route('survey.update', $survey->id) }}" method="POST">

								{!! csrf_field() !!}
								<input type="hidden" name="_method" value="PUT" />
		            
					            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
				                  <label>Title</label>
				                  <input type="text" name="title" value="{{{ $survey->title or old('title') }}}" class="form-control" />
				                </div>

				                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
					            	<label>Description</label>
					                <textarea name="description" class="form-control" placeholder="Place some text here">{{ $survey->description }}</textarea>
					            </div>

				                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
				                  <label>Status</label>
				                  <select name="status" class="form-control">
				                  	<option value="1" {{ $survey->status == 1 ? 'selected="selected"' : '' }}>Published</option>
				                  	<option value="0" {{ $survey->status == 0 ? 'selected="selected"' : '' }}>Deactivated</option>
				                  </select>
				                </div>

								<button class="btn btn-primary" type="submit">Update</button>
								<a href="{{ route('survey.index') }}" class="btn"><u>Cancel</u></a>

							</form>
						</div><!-- /.box-body -->
					</div><!-- /.box -->
				</div><!-- /.tab-pane -->

				<div class="tab-pane" id="tab_2">
					@include('survey/option_index')
				</div><!-- /.tab-pane -->

				<div class="tab-pane" id="tab_3">
					@include('survey/survey_result')
				</div><!-- /.tab-pane -->

			</div><!-- /.tab-content -->
		</div><!-- nav-tabs-custom -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
    <!-- DATA TABLES SCRIPT -->
	<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
      	$('#data_table tfoot th').each( function () {
	        var title = $('#data_table thead th').eq( $(this).index() ).text();
	        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	    } );

		$(function () {
			var table = $('#data_table').DataTable({
				"paging": true,
				"lengthChange": true,
				"searching": true,
				"ordering": true,
				"info": true,
				"autoWidth": true
			});

		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            that
		                .search( this.value )
		                .draw();
		        } );
		    } );
		});	
    </script>
@endsection
