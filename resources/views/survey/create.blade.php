@extends('admin_template')

@section('head')
@endsection

@section('breadcrumbs')
<li><a href="{{ route('survey.index') }}"><i class="fa fa-dashboard"></i> Survey List</a></li>
<li class="active">Create</li>
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('partials.flash_message')
		<div class="box">
			<div class="box-body">
				<form action="{{ route('survey.store') }}" method="POST">

					{!! csrf_field() !!}

					<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
						<label>Title</label>
						<input type="text" name="title" value="{{{ old('title') }}}" class="form-control" />
					</div>

					<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
						<label>Description</label>
						<textarea name="description" class="form-control" placeholder="Place some text here" ></textarea>
					</div>

					<button type="submit" class="btn btn-primary">Create Survey</button>
					<a href="{{ route('survey.index') }}" class="btn"><u>Cancel</u></a>

				</form>	    
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
@endsection