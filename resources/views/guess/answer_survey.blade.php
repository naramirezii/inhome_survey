@extends('public_template')

@section('head')
@endsection

@section('content')

<section id="blog" class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="blog">
                <div class="blog-item">
                    <div class="blog-content">

                        <h2>{{ $survey->title }}</h2>
                        @include('partials.flash_message')

                        @if(!is_null(session('answered_surveys')) && in_array($survey->id, session('answered_surveys')))
                            <div class="alert alert-info alert-dismissable">
                                <p>You have already answered this survey. Go back <a href="/">Home</a> to answer other surveys.</p>
                            </div>
                        @else
                            <form action="/answer-survey/{{$survey->id}}/store" method="POST">
                                {!! csrf_field() !!}

                                <div class="form-group {{ $errors->has('answer') ? 'has-error' : '' }}">
                                @foreach($survey->answerOptions as $option)
                                    <div class="radio">
                                      <label>
                                        <input type="radio" name="answer" value="{{ $option->id }}">
                                        {{ $option->title }}
                                      </label>
                                    </div>
                                @endforeach
                                </div>

                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                  <label>What's your name?</label>
                                  <input type="text" name="name" value="{{ old('name') }}" />
                                </div>

                                <button class="btn btn-primary" type="submit">Submit</button>
                            </form>
                        @endif
                        
                    </div>
                </div><!--/.blog-item-->
            </div>
        </div><!--/.col-md-8-->
    </div><!--/.row-->
</section><!--/#blog-->
	
@endsection

@section('script')
@endsection
