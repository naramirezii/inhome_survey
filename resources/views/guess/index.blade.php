@extends('public_template')

@section('content')
<section id="main-slider" class="no-margin">
    <div class="carousel slide wet-asphalt">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active" style="background-image: url(/img/slider/studying.jpg)">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="carousel-content centered">
                                <h2 class="boxed animation animated-item-1">ALPHA BETA C Surveys</h2>
                                <br/>
                                <p class="boxed animation animated-item-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item" style="background-image: url(/img/slider/business.jpg)">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="carousel-content center centered">
                                <h2 class="boxed animation animated-item-1">Lorem Ipsum</h2>
                                <br/>
                                <p class="boxed animation animated-item-2">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.carousel-inner-->
    </div><!--/.carousel-->
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="fa fa-angle-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="fa fa-angle-right"></i>
    </a>
</section><!--/#main-slider-->

<section id="surveys">
    <div class="container">
        @include('partials.flash_message')
        <h2>Current Surveys</h2>
        @forelse($surveys as $survey)
            <h4><a href="/answer-survey/{{$survey->id}}">{{ $survey->title }}</a></h4>
        @empty
            <p>No surveys available.</p>
        @endforelse
    </div>
</section><!--/#surveys-->
@endsection

@section('script')
@endsection