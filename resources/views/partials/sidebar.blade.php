<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>
            <li @if(Request::segment(1) == 'survey') class="active" @endif><a href="{{ url('survey') }}"><span>Survey Management</span></a></li>
        </ul><!-- /.sidebar-menu -->
        
    </section>
    <!-- /.sidebar -->
</aside>